'use strict';

angular.
  module('core').
  filter('checkavatar', function() {
    return function(input) {
      return input ? input : 'assets/img/avatar/default.jpg';
    };
  });
