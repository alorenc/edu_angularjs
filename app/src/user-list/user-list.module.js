'use strict';

// Define the `userList` module
angular.module('userList', ['ngAnimate', 'ngRoute', 'core', 'ngTable']);