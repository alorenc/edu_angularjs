'use strict';

// Register `userList` component, along with its associated controller and template
angular.
    module('userList').
    component('userList', {
        templateUrl: 'src/user-list/user-list.tpl.html',
        controller: ['$http', 'NgTableParams', userListController]
    });

function userListController($http, NgTableParams) {
    var self = this;

    $http.get('data/users.json').then(function (response) {
        self.data = response.data;
        self.tableParams = new NgTableParams({
            page: 1,
            count: 6,
            sorting: { name: "asc" }
        }, {
            counts: [], // 5,10,20
            dataset: response.data
        });
    })
};