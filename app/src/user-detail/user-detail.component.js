'use strict';

angular.
    module('userDetail').
    component('userDetail', {
        templateUrl: 'src/user-detail/user-detail.tpl.html',
        controller: ['$scope', '$routeParams', '$http', userDetailController]
    });

function userDetailController($scope, $routeParams, $http) {
    var self = this;
    $scope.errorMsg = '';
        
    $http.get('data/users.json').then(function (response) {
        var user = response.data.filter(item => item.id === parseInt($routeParams.userId));
        
        if(user.length === 0) {
            $scope.errorMsg = 'Not Found user id: ' + $routeParams.userId;
        } else {
            self.user = user[0];
        }
    });
};