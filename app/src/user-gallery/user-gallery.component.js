'use strict';

angular.
    module('userGallery').
    component('userGallery', {
        templateUrl: 'src/user-gallery/user-gallery.tpl.html',
        controller: ['$scope', '$http', '$routeParams', '$log', userGalleryController]
    });

function userGalleryController($scope, $http, $routeParams, $log) {
    var self = this;
    $scope.errorMsg = '';
    self.orderProp = '-id';
    
    $http.get("http://jsonplaceholder.typicode.com/albums?userId="+$routeParams.userId).then(function (response) {
        $scope.galleries = response.data;

        if ($scope.galleries.length === 0) {
            $scope.errorMsg = 'User has not albums defined';
        } else {
            self.albumId = (self.albumId > 0) ? self.albumId : $scope.galleries[0].id;
            self.getPhotos(self.albumId);
        }
    });

    self.getPhotos = function getPhotos(albumid) {
        self.albumObj = $scope.galleries.filter(item => item.id == albumid)[0];
        self.resetFilters();

        $http.get('http://jsonplaceholder.typicode.com/photos').then(function (response) {
            $scope.photos = response.data.filter(item => item.albumId === albumid);
        });
    };

    self.resetFilters = function resetFilters () {
		self.query = '';
    };
    
    self.addAlbum = function addAlbum(name) {
        createAlbum(name);
    }

    self.removeImage = function removeImage(photoId) {
        var isConfirmed = confirm("Are you sure to delete this image ?");
        if (isConfirmed) {
            deletePhotos(photoId);
        } else {
            return false;
        }
    };

    var createAlbum = function createAlbum(name) {
        var data = {userId: parseInt($routeParams.userId), title: name};

        $http.post('http://jsonplaceholder.typicode.com/albums', data).then(function (response) {
            $log.debug("Add fake album: code: " + response.status + ", status: " + response.statusText)
            
            if(response.status == 201) {
                $scope.galleries[$scope.galleries.length] = response.data;
                $scope.name = '';
            } else {
                alert("Album not added: " + response.statusText);
            }
        });
    };

    var deletePhotos = function deletePhotos(photoId) {
        $http.delete('http://jsonplaceholder.typicode.com/photos/'+photoId).then(function (response) {
            $log.debug("Delete fake photo: code: " + response.status + ", status: " + response.statusText)

            if(response.status == 200) {
                $scope.photos = $scope.photos.filter(item => item.id !== photoId);
            } else {
                alert("Photo not deleted: " + response.statusText);
            }
        });
    };
};