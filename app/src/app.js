'use strict';

// Define the `galleryApp` module
angular.module('galleryApp', [
    'ngAnimate',
    'ngRoute',
    'ngTable',
    'core',
    'userList',
    'userDetail',
    'userGallery'
]);